package com.gs.ria.services;

import com.gs.ria.domain.User;

public interface UserManagerService {
    void save(User user);
}
