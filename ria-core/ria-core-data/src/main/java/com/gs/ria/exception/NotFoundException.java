package com.gs.ria.exception;

public class NotFoundException extends Exception {

    public NotFoundException(final String message) {
        super(message);
    }

    public NotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
