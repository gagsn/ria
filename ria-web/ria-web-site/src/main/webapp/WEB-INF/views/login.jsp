<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login page</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<spring:url value="/resources/css/ria.css"/>" type="text/css"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="<spring:url value="/resources/js/ria.js"/>" type="application/javascript"></script>


</head>
<body>

<jsp:include page="../views/fragments/header.jsp"/>

<div class="container">
    <div class="row">
        <h1>Services</h1>
    </div>
    <c:url value="/login" var="loginVar"/>
    <form id="appointment-form" action="${loginVar}" method="POST">
        <div class="form-group">
            <label for="make">Username</label>
            <input name="custom_username" class="form-control" />
        </div>
        <div class="form-group">
            <label for="model">Password</label>
            <input type="password" name="custom_password" class="form-control" />
        </div>
        <sec:csrfInput/>

        <c:if test="${param.logout != null }">
            <p>You have successfully been logged out.</p>
        </c:if>

        <c:if test="${param.error != null}">
            <p class="authentication-failed">Invalid user name or password.</p>
        </c:if>

        <button type="submit" id="btn-save" class="btn btn-primary">Login</button>
    </form>
</div>
</body>
</html>