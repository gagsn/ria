<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; UTF-8">
    <title>Project Manager</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="<spring:url value="/resources/css/ria.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<spring:url value="/resources/css/bootstrap-select.min.css"/>" type="text/css"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="<spring:url value="/resources/js/bootstrap-select.min.js"/>"></script>
    <script src="<spring:url value="/resources/js/ria.js"/>" type="application/javascript"></script>

</head>
<body>

<jsp:include page="../views/fragments/header.jsp"/>

<div class="container">
    <div class="row">

        <form action="<spring:url value="/project/add"/>" method="post" class="col-md-8 col-md-offset-2">

            <div class="form-group">
                <label for="project-name">Name</label>
                <input type="text" id="project-name"
                       class="form-control" name="name"/>
            </div>

            <div class="form-group">
                <label for="project_type">Type</label>
                <select id="project_type" name="type" class="selectpicker">
                    <option></option>
                    <option value="single">Single Year</option>
                    <option value="multi">Multi-Year</option>
                </select>
            </div>

            <div class="form-group">
                <label for="sponsor">Sponsor</label>
                <input id="sponsor" type="text"
                       class="form-control" name="sponsor"/>
            </div>

            <div class="form-group">
                <label for="funds">Authorized Funds</label>
                <input id="funds" type="text"
                       class="form-control" name="authorized_funds"/>
            </div>

            <div class="form-group">
                <label for="hours">Authorized Hours</label>
                <input id="hours" type="text"
                       class="form-control" name="authorized_hours"/>
            </div>

            <div class="form-group">
                <label for="project-name">Description</label>
                <textarea class="form-control" rows="3"></textarea>
            </div>

            <div class="form-group">
                <label for="special">Special</label>
                <input id="special" name="special" type="checkbox"/>
            </div>

            <sec:csrfInput/>
            <button type="submit" class="btn btn-default">Submit</button>

        </form>

    </div>
</div>
</body>
</html>