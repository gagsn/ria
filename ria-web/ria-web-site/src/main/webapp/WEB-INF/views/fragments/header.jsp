<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:url value="/" var="home"/>
<c:url value="/login" var="login"/>
<c:url value="/logout" var="logout"/>
<c:url value="/project/add" var="addProject"/>

<nav class="navbar navbar-default">
    <div class="container-fluid">

        <div class="navbar-header">
            <a href="<spring:url value="${home}"/>" class="navbar-brand">Ria project</a>
        </div>

        <ul class="nav navbar-nav">

            <li><a href="<spring:url value="${home}"/>">Home</a></li>

            <li class="dropdown">

                <a href="#" class="dropdown-toggle"
                   data-toggle="dropdown" role="button"
                   aria-expanded="false">Projects <span class="caret"></span></a>

                <ul class="dropdown-menu" role="menu">
                    <li><a href="<spring:url value="${addProject}"/>">Add</a></li>
                    <li><a href="#">Find</a></li>
                </ul>

            </li>

            <li class="dropdown">

                <a href="#" class="dropdown-toggle"
                   data-toggle="dropdown" role="button"
                   aria-expanded="false">Resources <span class="caret"></span></a>

                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Add</a></li>
                    <li><a href="#">Find</a></li>
                </ul>

            </li>

            <sec:authorize access="authenticated" var="authenticated"/>
            <c:choose>
                <c:when test="${authenticated}">
                    <li>
                        <p class="navbar-text">
                            Welcome
                            <sec:authentication property="name"/>
                            <a id="logout" href="#">Logout</a>
                        </p>
                        <form id="logout-form" action="${logout}" method="post">
                            <sec:csrfInput/>
                        </form>
                    </li>
                </c:when>

                <c:otherwise>
                    <li><a href="<spring:url value="${login}"/>">Sign In</a></li>
                </c:otherwise>
            </c:choose>

        </ul>

    </div>
</nav>